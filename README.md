# DNS Failover Service

A poor man's DNS failover service written in GoLang.

In case you have multiple `A` records assigned to your domain  
you can let this service monitor the current status.

If one of the addresses is no longer reachable it will be automatically  
removed from the zone file.

Currently we support following provider:

* Gandi (https://www.gandi.net)

If you want to extend this list have a look at `services/service.go`  
and `services/gandi.go` for reference!

## Run the service

Copy the configuration file and add required information to it:

```
cp config.toml.example config.toml
```

Install dependencies and compile the project:

```
go get -u github.com/golang/dep/cmd/dep
dep ensure
go build .
```

Run the binary! Enjoy :)

## Docker

You can build the docker image by executing:

```
cd docker && docker build -t feneas/dns-failover:latest .
```

### Environment Vars

| Variable | Description |
| -------- | ----------- |
| CONFIG | Pass dns-failover config as variable |
| CONFIG_FILE | Mounted configuration file |

Alternatively you can simply mount the config directly as `/home/dnsf/config.toml`.
