package main
//
// DNS Failover Service
// Copyright (C) 2019 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "net"
  "time"
  "github.com/tatsushid/go-fastping"
  "git.feneas.org/feneas/infrastructure/dns-failover/services"
  "git.feneas.org/feneas/infrastructure/dns-failover/notifications"
  "fmt"
)

func worker(config TomlConfig, services services.Services, notifiers notifications.Notifications) {
  logger.Println("Started worker process")

  hostsUp := make(map[string]bool)
  for {
    for _, address := range config.Addresses {
      pinger := fastping.NewPinger()
      pinger.AddIP(address)
      pinger.OnRecv = func(addr *net.IPAddr, rtt time.Duration) {
        hostsUp[address] = true
      }
      hostsUp[address] = false

      err := pinger.Run()
      if err != nil {
        logger.Printf("%s: %+v\n", address, err)
        continue
      }
    }

    var errCnt = 0
    for address, up := range hostsUp {
      ip := net.ParseIP(address)
      if ip == nil {
        logger.Printf("Cannot parse IP: %s\n", address)
        continue
      }

      for _, service := range services {
        exists, err := service.ExistsA(ip)
        if err != nil {
          errCnt += 1
          if errCnt > 3 {
            // not every time-out should trigger an alert
            notifiers.Warn(err.Error())
            errCnt = 0
          }
          logger.Printf("Something went wrong: %+v\n", err)
          continue
        }
        errCnt = 0 // reset error count if it succeeded

        if exists && !up {
          errMsg := fmt.Sprintf(
            "%s exists and is down: removing record\n", address)
          logger.Printf(errMsg)

          err := service.RemoveA(ip)
          if err != nil {
            notifiers.Warn(err.Error())
            logger.Printf("%s: %+v\n", address, err)
            continue
          }

          errs := notifiers.Info(errMsg)
          if len(errs) > 0 {
            logger.Printf("notifiers.Warn: %+v\n", errs)
          }
        }

        if !exists && up {
          errMsg := fmt.Sprintf(
            "%s doesn't exist and is up: adding record\n", address)
          logger.Printf(errMsg)

          err := service.AddA(ip)
          if err != nil {
            notifiers.Warn(err.Error())
            logger.Printf("%s: %+v\n", address, err)
            continue
          }

          errs := notifiers.Info(errMsg)
          if len(errs) > 0 {
            logger.Printf("notifiers.Info: %+v\n", errs)
          }
        }
      }
    }
    // sleep between running service checks
    time.Sleep(time.Duration(config.Timeout) * time.Second)
  }
}
