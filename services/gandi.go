package services
//
// DNS Failover Service
// Copyright (C) 2019 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "net"
  "net/http"
  "fmt"
  "io"
  "io/ioutil"
  "encoding/json"
  "bytes"
  "errors"
  "strings"
)

type GandiApiUrl string

const (
  GandiV5API GandiApiUrl = "https://dns.api.gandi.net/api/v5"
)

type GandiService struct{
  token, rootDomain string
  endpoint *GandiApiUrl
}

type GandiServiceDomain struct {
  ZoneUuid string `json:"zone_uuid"`
  DomainKeysHref string `json:"domain_keys_href"`
  Fqdn string `json:"fqdn"`
  ZoneHref string `json:"zone_href"`
  ZoneRecordsHref string `json:"zone_records_href"`
  DomainRecordsHref string `json:"domain_records_href"`
  DomainHref string `json:"domain_href"`
}

type GandiServiceRecord struct {
  Type string `json:"rrset_type"`
  TTL int `json:"rrset_ttl"`
  Name string `json:"rrset_name"`
  Href string `json:"rrset_href"`
  Values []string `json:"rrset_values"`
}

func (u GandiApiUrl) String() string {
  return string(u)
}

func (s *GandiService) New(token, rootDomain string, url GandiApiUrl) *GandiService {
  s.rootDomain = rootDomain
  s.token = token
  s.endpoint = &url

  return s
}

func (s *GandiService) AddA(ip net.IP) error {
  record, err := s.fetchAllA()
  if err != nil {
    return err
  }

  record.Values = append(record.Values, ip.String())
  return s.pushAllA(record)
}

func (s *GandiService) RemoveA(ip net.IP) error {
  record, err := s.fetchAllA()
  if err != nil {
    return err
  }

  for i, address := range record.Values {
    if ip.String() == address {
      if len(record.Values) == (i + 1) {
        record.Values = record.Values[:i]
      } else {
        record.Values = append(record.Values[:i], record.Values[i+1:]...)
      }
    }
  }
  return s.pushAllA(record)
}

func (s *GandiService) ExistsA(ip net.IP) (bool, error) {
  record, err := s.fetchAllA()
  if err != nil {
    return false, err
  }

  for _, address := range record.Values {
    if ip.String() == address {
      return true, nil
    }
  }
  return false, nil
}

func (s *GandiService) pushAllA(record GandiServiceRecord) error {
  var domain GandiServiceDomain
  err := s.request(http.MethodGet,
    fmt.Sprintf("/domains/%s", s.rootDomain), &domain, nil)
  if err != nil {
    return err
  }

  payload, err := json.Marshal(record)
  if err != nil {
    return err
  }

  return s.request(http.MethodPut,
    fmt.Sprintf("%s/%%40/A", domain.ZoneRecordsHref),
    struct{}{}, bytes.NewReader(payload))
}

func (s *GandiService) fetchAllA() (record GandiServiceRecord, err error) {
  var domain GandiServiceDomain
  err = s.request(http.MethodGet,
    fmt.Sprintf("/domains/%s", s.rootDomain), &domain, nil)
  if err != nil {
    return
  }

  err = s.request(http.MethodGet,
    fmt.Sprintf("%s/%%40/A", domain.ZoneRecordsHref), &record, nil)
  return
}

func (s *GandiService) request(method, path string, res interface{}, rea io.Reader) error {
  if !strings.HasPrefix(path, "http") {
    path = fmt.Sprintf("%s/%s", s.endpoint, path)
  }

  req, err := http.NewRequest(method, path, rea)
  if err != nil {
    return err
  }

  // set some standard headers
  req.Header.Set("User-Agent", UserAgent)
  req.Header.Set("X-Api-Key", s.token)
  req.Header.Set("Content-Type", "application/json")

  client := &http.Client{}
  resp, err := client.Do(req)
  if err != nil {
    return err
  }
  defer resp.Body.Close()

  if resp.StatusCode < 200 || resp.StatusCode >= 300 {
    return errors.New("HTTP status " + resp.Status)
  }

  body, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    return err
  }

  return json.Unmarshal(body, &res)
}
