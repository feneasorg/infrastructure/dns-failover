package notifications
//
// DNS Failover Service
// Copyright (C) 2019 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "bytes"
  "encoding/json"
  "net/http"
  "errors"
)

type T2BotFormat string

type T2BotWebhook struct {
  Text string `json:"text"`
  Format T2BotFormat `json:"format"`
  DisplayName *string `json:"displayName,omitempty"`
  AvatarUrl *string `json:"avatarUrl,omitempty"`

  url string `json:"-"`
}

const (
  T2BotPlainText T2BotFormat = "plain"
)

func (w *T2BotWebhook) New(url string, name, avatar *string) *T2BotWebhook {
  w.url = url
  w.DisplayName = name
  w.AvatarUrl = avatar
  return w
}

func (w *T2BotWebhook) Info(msg string) error {
  w.Text = msg
  w.Format = T2BotPlainText
  return w.send()
}

func (w *T2BotWebhook) Warn(msg string) error {
  w.Text = msg
  w.Format = T2BotPlainText
  return w.send()
}

func (w T2BotWebhook) send() error {
  payload, err := json.Marshal(w)
  if err != nil {
    return err
  }

  req, err := http.NewRequest("POST", w.url, bytes.NewReader(payload))
  if err != nil {
    return err
  }
  req.Header.Set("User-Agent", UserAgent)
  req.Header.Set("Content-Type", "application/json")

  client := &http.Client{}
  resp, err := client.Do(req)
  if err != nil {
    return err
  }
  defer resp.Body.Close()

  if !(resp.StatusCode == 200 || resp.StatusCode == 202) {
    return errors.New("HTTP status " + resp.Status)
  }
  return nil
}
