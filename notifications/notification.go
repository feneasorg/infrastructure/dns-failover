package notifications
//
// DNS Failover Service
// Copyright (C) 2019 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import "fmt"

const (
  UserAgent string = "DNSFailoverAgent/v1.0"
)

type Notification interface {
  Info(string) error
  Warn(string) error
}

type Notifications []Notification

func (n Notifications) Info(msg string) (errs []error) {
  msg = fmt.Sprintf("INFO: %s", msg)
  for _, notifier := range n {
    err := notifier.Info(msg)
    if err != nil {
      errs = append(errs, err)
    }
  }
  return
}

func (n Notifications) Warn(msg string) (errs []error) {
  msg = fmt.Sprintf("WARNING: %s", msg)
  for _, notifier := range n {
    err := notifier.Warn(msg)
    if err != nil {
      errs = append(errs, err)
    }
  }
  return
}
