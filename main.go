package main
//
// DNS Failover Service
// Copyright (C) 2019 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import(
  "os"
  "log"
  "git.feneas.org/feneas/infrastructure/dns-failover/services"
  "git.feneas.org/feneas/infrastructure/dns-failover/notifications"
  "github.com/BurntSushi/toml"
)

type TomlConfig struct {
  Timeout uint `toml:"timeout"`
  Addresses []string `toml:"addresses"`
  Services map[string]map[string]string
  Notifications map[string]map[string]string
}

var logger = log.New(os.Stdout, "", log.Lshortfile)

func main() {
  var config TomlConfig
  if _, err := toml.DecodeFile("config.toml", &config); err != nil {
    logger.Println(err)
    os.Exit(1)
  }

  var jobs services.Services
  for key, serviceMap := range config.Services {
    switch key {
    case "gandi":
      var gandi services.GandiService
      token, ok := serviceMap["api_token"]; if !ok {
        logger.Println("[services.%s] missing api_token\n", key)
        continue
      }
      rootDomain, ok := serviceMap["root_domain"]; if !ok {
        logger.Println("[services.%s] missing root_domain\n", key)
        continue
      }
      gandi.New(token, rootDomain, services.GandiV5API)
      jobs = append(jobs, &gandi)
    default:
      logger.Printf("Unknown service [services.%s]\n", key)
    }
  }

  var notifiers notifications.Notifications
  for key, notificationMap := range config.Notifications {
    switch key {
    case "t2bot_webhook":
      var t2bot notifications.T2BotWebhook
      url, ok := notificationMap["url"]; if !ok {
        logger.Println("[notifications.%s] missing url\n", key)
        continue
      }
      name, ok := notificationMap["display_name"]; if !ok {
        logger.Println("[notifications.%s] missing display_name\n", key)
        continue
      }
      avatarUrl, ok := notificationMap["avatar_url"]; if !ok {
        logger.Println("[notifications.%s] missing avatar_urll\n", key)
        continue
      }
      t2bot.New(url, &name, &avatarUrl)
      notifiers = append(notifiers, &t2bot)
    default:
      logger.Printf("Unknown notification [notifications.%s]\n", key)
    }
  }

  worker(config, jobs, notifiers)
}
