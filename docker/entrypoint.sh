#!/bin/sh

appconf="/home/dnsf/config.toml"

if [ "$CONFIG" != "" ]; then
  echo -ne "$CONFIG" > $appconf
fi

if [ "$CONFIG_FILE" != "" ]; then
  cp -v $CONFIG_FILE $appconf
fi

/home/dnsf/dns-failover $@
